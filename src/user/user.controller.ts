import { Body, Controller, Get, Param, Post, Put } from '@nestjs/common';
import { UserService } from './user.service';
import { User } from './user.model';

@Controller('user')
export class UserController {
  constructor(private service: UserService) {}

  @Get(':id')
    async find(@Param('id') id: string) {
    return this.service.find(id);
  }

  @Post('create')
  create(@Body() user: User) {
    return this.service.create(user);
  }

  @Put('update')
  update(@Body() user: User) {
    return this.service.update(user);
  }

}