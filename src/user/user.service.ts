import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { User } from './user.model';
import { Model } from 'mongoose';

@Injectable()
export class UserService {
  constructor(@InjectModel('User') private readonly userModel: Model<User>) {}

  async create(doc: User) {
    console.log(doc);
    const result = await new this.userModel(doc).save();
    return result.id;
  }

  async find(id: string) {
    return await this.userModel.find({category: id}).exec();
  }
  
  async update(user: User) {
    return await this.userModel.findByIdAndUpdate(user);
  }

}