import * as mongoose from 'mongoose';

export const UserSchema = new mongoose.Schema({
  name: { type: String, required: false },
  endereco: { type: String, required: false },
  cpfOrCnpj: { type: String, required: false },
  category: { type: String, required: false },
  description: { type: String, required: false },
  telefone: { type: String, required: false },

});

export interface User {
  name: string;
  endereco: string;
  category: string;
  cpfOrCnpj: string;
  description: string;
  telefone: string
}